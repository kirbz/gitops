<domain type='kvm'>
  
  <!-- Metadata
       https://libvirt.org/formatdomain.html#elementsMetadata -->
  <!-- This name should consist only of alpha-numeric characters and is required to be unique within the scope of a single host. -->
  <name>{{system['machine'].hostname}}</name>
  <!-- The optional element title provides space for a short description of the domain. The title should not contain any newlines. -->
  <title>{{system['machine'].hostname}}</title>
  <!-- This is not used by libvirt in any way, it can contain any information the user wants. 
  <description>{{config['libvirt'].vm_description}}</description> -->


  <!-- RAM 
       https://libvirt.org/formatdomain.html#elementsMemoryAllocation -->
  <!-- The maximum allocation of memory for the guest at boot time. Includes possible additional memory devices specified at start or hotplugged later. -->
  <memory unit='G'>{{config['libvirt'].memory}}</memory>
  <!-- The actual allocation of memory for the guest. Can be less than the maximum allocation, to allow for ballooning up the guests memory on the fly. 
       If this is omitted, it defaults to the same value as the memory element. -->
  <currentMemory unit='G'>{{config['libvirt'].memory}}</currentMemory>


  <!-- CPU
       https://libvirt.org/formatdomain.html#elementsCPUAllocation -->
  <vcpu placement='static'>{{config['libvirt'].vcpu}}</vcpu>
  

  <!-- Operating System
       https://libvirt.org/formatdomain.html#elementsOS -->
  <os>
    <!-- The type of operating system to be booted in the virtual machine. hvm indicates that the OS is one designed to run on bare metal, so requires full virtualization. -->
    <type arch='x86_64' machine='pc-i440fx-2.4'>hvm</type>

    <!-- Enables or disables Serial Graphics Adapter which allows users to see BIOS messages on a serial port 
	 rebootTimeout controls how long the guest should start booting again in case the boot fails (according to BIOS). The value is in milliseconds, -1 disables the reboot.-->
    <bios useserial='yes' rebootTimeout='-1'/>

    <!-- The fully-qualified path to the kernel image in the host OS -->
    <kernel>/var/lib/libvirt/kernels/gentoo-sources</kernel>
    <cmdline>{{system['bootloader'].bootloader_cmdline}}</cmdline>
  </os>
  

  <!-- Hypervisor Features 
       https://libvirt.org/formatdomain.html#elementsFeatures -->
  <features>
    <!-- ACPI is useful for power management, for example, with KVM guests it is required for graceful shutdown to work. -->
    <acpi/>
    <!-- APIC allows the use of programmable IRQ management. -->
    <apic/>
    <!-- Physical address extension mode allows 32-bit guests to address more than 4 GB of memory. -->
    <pae/>
    <!-- Notify the guest that the host supports paravirtual spinlocks for example by exposing the pvticketlocks mechanism. -->
    <pvspinlock/>
    <!-- Enable or disable the emulation of VMware IO port, for vmmouse etc. -->
    <vmport state='off'/>
    <!-- Enable or disable the performance monitoring unit for the guest. -->
    <pmu state='off'/>
  </features>


  <!-- CPU Features 
       https://libvirt.org/formatdomain.html#elementsCPU -->
  <!-- host-model is a shortcut to copying host CPU definition from capabilities XML into domain XML -->
  <cpu mode='host-model'>
    <!-- If a hypervisor is not able to use the exact CPU model, libvirt automatically falls back to a closest model supported by the hypervisor -->
    <model fallback='allow'/>
  </cpu>


  <!-- Clock
       https://libvirt.org/formatdomain.html#elementsFeatures -->
  <!-- The guest clock will always be synchronized to UTC when booted. -->
  <clock offset='utc'>
    <!-- Specify different Timers.
	 tickpolicy attribute determines what happens when QEMU misses a deadline for injecting a tick to the guest
	   delay - Continue to deliver ticks at the normal rate. The guest time will be delayed due to the late tick
	   catchup - Deliver ticks at a higher rate to catch up with the missed tick. The guest time should not be delayed once catchup is complete. -->
    <timer name='rtc' tickpolicy='catchup'/>
    <timer name='pit' tickpolicy='delay'/>
    <!-- The present attribute can be "yes" or "no" to specify whether a particular timer is available to the guest. -->
    <timer name='hpet' present='no'/>
  </clock>


  <!-- Events
       https://libvirt.org/formatdomain.html#elementsEvents -->
  <!-- The content of this element specifies the action to take when the guest requests a poweroff. -->
  <on_poweroff>destroy</on_poweroff>
  <!-- The content of this element specifies the action to take when the guest requests a reboot. -->
  <on_reboot>restart</on_reboot>
  <!-- The content of this element specifies the action to take when the guest crashes. -->
  <on_crash>restart</on_crash>


  <!-- Power Management
       https://libvirt.org/formatdomain.html#elementsPowerManagement -->
  <pm>
    <!-- These elements enable ('yes') or disable ('no') BIOS support for S3 (suspend-to-mem) and S4 (suspend-to-disk) ACPI sleep states. -->
    <suspend-to-mem enabled='no'/>
    <suspend-to-disk enabled='no'/>
  </pm>


  <!-- Devices
       https://libvirt.org/formatdomain.html#elementsDevices -->
  <devices>

    <!-- Specify the fully qualified path to the device model emulator binary. -->
    <emulator>/usr/bin/qemu-system-x86_64</emulator>

{%- for k1, disk in system['disks'].items() %}
    {%- if disk.machine_disk_device is defined %}
    <!-- Any device that looks like a disk, be it a floppy, harddisk, cdrom, or paravirtualized driver is specified via the disk element. 
  	   Possible values for the type attribute are: "file", "block", "dir" and "volume"
  	   Possible values for the device attribute are: "floppy", "disk", "cdrom", and "lun", defaulting to "disk". -->
    <disk type='file' device='disk'>
      
      <!-- The optional driver element allows specifying further details related to the hypervisor driver used to provide the disk. -->
      <driver name='qemu' type='{{disk.format}}' cache='{{disk.cache}}'/>
      
      <!-- The file attribute specifies the fully-qualified path to the file holding the disk. 
	   The name attribute selects the primary backend driver name 
	   The optional type attribute is the type of the file, e.g. "raw", "qcow2"
	   The optional cache attribute controls the cache mechanism, e.g. "default", "none", "writethrough", "writeback" -->
      <source file='/var/lib/libvirt/images/{{system['machine'].hostname}}/{{system['machine'].hostname}}_{{disk.name}}.{{disk.format}}'/>
      
      <!-- The target element controls the bus / device under which the disk is exposed to the guest OS. 
	   The optional bus attribute specifies the type of disk device to emulate, "ide", "scsi", "virtio", "xen", "usb", "sata" -->
      <target dev='{{ disk.machine_disk_device | replace("/dev/","") }}' bus='virtio'/>

      {%- if disk.name == "boot" %}
      <!-- Specifies that the disk is bootable. The order attribute determines the order in which devices will be tried during boot sequence. -->
      <boot order='1'/>
      {%- endif %}

    </disk>
    {% endif %}
{%- endfor %}


    {%- if system['installer'].download_install_iso == true %}
    <disk type='file' device='cdrom'>
      <driver name='qemu' type='raw'/>
      <source file='/var/lib/libvirt/images/{{system['machine'].hostname}}/latest-minimal.iso'/>
      <target dev='sda' bus='sata'/>
      <readonly/>
    </disk>
    {% endif %}

    <!-- Controllers
	 https://libvirt.org/formatdomain.html#elementsControllers 
	 Depending on the guest architecture, some device buses can appear more than once, with a group of virtual devices tied to a virtual controller. 
	 Normally, libvirt can automatically infer such controllers without requiring explicit XML markup, but sometimes it is necessary to provide an 
	 explicit controller element, notably when planning the PCI topology for guests where device hotplug is expected. 
	 
	 Attribute "type" must be one of 'ide', 'fdc', 'scsi', 'sata', 'usb', 'ccid', 'virtio-serial' or 'pci' 

	 Attribute "index" (optional) is the decimal integer describing in which order the bus controller is encountered (for use in controller attributes of <address> elements)
	 if not specified, it will be auto-assigned to be the lowest unused index for the given controller type.
 

         A USB Controller 
	  - has an optional attribute model, which is one of "piix3-uhci", "piix4-uhci", "ehci", "ich9-ehci1", "ich9-uhci1", "ich9-uhci2", 
   	    "ich9-uhci3", "vt82c686b-uhci", "pci-ohci", "nec-xhci", or "qemu-xhci".  If the USB bus needs to be explicitly disabled for the guest, model='none'.
	  - accepts a ports attribute to configure how many devices can be connected to the controller.

         USB Companion Controllers have an optional sub-element <master> to specify the exact relationship of the companion to its master controller. 
	 A companion controller is on the same bus as its master, so the companion index value should be equal.

        
	 A PCI Controller
	  - has an optional model attribute with possible values pci-root, pcie-root, pcie-root-port, pci-bridge, dmi-to-pci-bridge, pcie-switch-upstream-port, 
	    pcie-switch-downstream-port, pci-expander-bus, or pcie-expander-bus.
	  - When attaching Devices to the PCI Controller using the <address> sub-element (e.g. Sound, Video, Disk Devices), PCI addresses have the following 
	    additional attributes: 
	      - domain (a 2-byte hex integer, not currently used by qemu)
	      - bus (a hex value between 0 and 0xff, inclusive)
	      - slot (a hex value between 0x0 and 0x1f, inclusive)
	      - function (a value between 0 and 7, inclusive).


         A virtio-serial controller has two additional optional attributes ports and vectors, which control how many devices can be connected through the controller. -->

    <!-- USB Controllers and Companion Controllers 
    <controller type='usb' index='0' model='ich9-ehci1'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x7'/>
    </controller>
    <controller type='usb' index='0' model='ich9-uhci1'>
      <master startport='0'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x0' multifunction='on'/>
    </controller>
    <controller type='usb' index='0' model='ich9-uhci2'>
      <master startport='2'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x1'/>
    </controller>
    <controller type='usb' index='0' model='ich9-uhci3'>
      <master startport='4'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x2'/>
    </controller> -->

    <!-- PCI Controller -->
    <controller type='pci' index='0' model='pci-root'/>
    
    <!-- Serial Controller -->
    <controller type='virtio-serial' index='0'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x05' function='0x0'/>
    </controller>

    
    <!-- Bridge Networking
	 https://libvirt.org/formatdomain.html#elementsNICSBridge -->
    <!-- This is the recommended config for general guest connectivity on hosts with static wired networking configs. 
	 Provides a bridge from the VM directly to the LAN. This assumes there is a bridge device on the host which has one or more of the hosts physical NICs enslaved.
	 The IP range / network configuration is whatever is used on the LAN. This provides the guest VM full incoming & outgoing net access just like a physical machine.
	 -->
    <interface type='bridge'>
      <!-- Packets whose destination is on the same host as where they originate from are directly delivered to the target macvtap device. 
	   Both origin and destination devices need to be in bridge mode for direct delivery. -->
      <source bridge='br0'/>
      <model type='virtio'/>
      <mac address='52:54:00:c7:84:05'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
    </interface>


    <!-- Serial Port
	 https://libvirt.org/formatdomain.html#elementCharSerial -->
    <serial type='pty'>
      <!-- Target can have a port attribute, which specifies the port number. Ports are numbered starting from 0. There are usually 0, 1 or 2 serial ports. -->
      <target port='0'/>
    </serial>

    <!-- Console
	 https://libvirt.org/formatdomain.html#elementCharConsole
	 The console element is used to represent interactive consoles. Depending on the type of guest in use, the consoles might be paravirtualized devices, 
	 or they might be a clone of a serial device.  If the console is presented as a serial port, the target element has the same attributes as for a serial port. 
	 There is usually only 1 console. -->
    <console type='pty'>
      <target port='0'/>
    </console>

    <!-- Channel
	 https://libvirt.org/formatdomain.html#elementCharChannel
	 This represents a private communication channel between the host and the guest. -->

    <!-- Paravirtualized SPICE channel. The domain must also have a SPICE server as a graphics device. 
    <channel type='spicevmc'>
      <target type='virtio' name='com.redhat.spice.0'/>
      <address type='virtio-serial' controller='0' bus='0' port='1'/>
    </channel> -->


    <!-- Input Devices
         https://libvirt.org/formatdomain.html#elementsInput
	 Input devices allow interaction with the graphical framebuffer in the guest virtual machine. 
	 When enabling the framebuffer, an input device is automatically provided. 
    <input type='tablet' bus='usb'/>
    <input type='mouse' bus='ps2'/>
    <input type='keyboard' bus='ps2'/> -->
    

    <!-- Graphics Framebuffers 
	 https://libvirt.org/formatdomain.html#elementsGraphics
	 A graphics device allows for graphical interaction with the guest OS. A guest will typically have either a framebuffer or a text console configured 
	 to allow interaction with the admin. -->

    <!-- Type can take the value of sdl, vnc, spice, rdp or desktop 
	 Starting a Spice enables virt-manager to connect.  Spice supports variable compression settings for audio, images and streaming 
    <graphics type='spice' autoport='yes' listen='127.0.0.1' keymap='en-gb'>
      <listen type='address' address='127.0.0.1'/>
      <image compression='auto_lz'/>
      <jpeg compression='always'/>
      <zlib compression='always'/>
      <playback compression='off'/>
      <streaming mode='filter'/>
    </graphics> -->
    

    <!-- Sound Device
	 https://libvirt.org/formatdomain.html#elementsSound
	 A virtual sound card can be attached to the host via the sound element. -->

    <!-- model specifies what real sound device is emulated, values 'es1370', 'sb16', 'ac97', 'ich6' and 'usb' 
    <sound model='ich6'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x04' function='0x0'/>
    </sound> -->

    
    <!-- Video Device
	 https://libvirt.org/formatdomain.html#elementsVideo 
    <video>
      <model type='qxl' ram='65536' vram='65536' vgamem='16384' heads='1'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
    </video> -->


    <!-- Redirected Devices
	 https://libvirt.org/formatdomain.html#elementsRedir
	 USB device redirection through a character device is supported
    <redirdev bus='usb' type='spicevmc'>
    </redirdev>
    <redirdev bus='usb' type='spicevmc'>
    </redirdev> -->


    <!-- Random Number Generator 
	 https://libvirt.org/formatdomain.html#elementsRng -->
    <rng model='virtio'>
      <rate period="2000" bytes="1234"/>
      <backend model='random'>/dev/random</backend>
    </rng>


    <!-- Memory Balloon Device
	 https://libvirt.org/formatdomain.html#elementsMemBalloon -->
    <memballoon model='virtio'>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x09' function='0x0'/>
    </memballoon>

  </devices>

</domain>
